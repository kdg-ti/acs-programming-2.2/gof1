import observer.Point;
import observer.PointObserver;

public class Demo {
    public static void main(String[] args) {
        Point punt = new Point(1, 2);
        PointObserver observer = new PointObserver();
        punt.addObserver(observer);

        punt.doubleX();
        punt.doubleY();
    }
}
