package observer;

import java.util.Observable;

public class Point extends Observable {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void doubleX() {
        x *= 2;
        setChanged();
        notifyObservers("X");
    }

    public void doubleY() {
        y *= 2;
        setChanged();
        notifyObservers("Y");
    }

    @Override
    public String toString() {
        return "(x,y) = (" + x + "," + y + ")";
    }
}
