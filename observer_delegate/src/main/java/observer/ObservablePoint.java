package observer;

import java.util.Observable;

public class ObservablePoint extends Observable {
    private Point point;

    public ObservablePoint(int x, int y) {
       point = new Point(x, y);
    }

    public void doubleX() {
        point.doubleX();
        setChanged();
        notifyObservers("X");
    }

    public void doubleY() {
        point.doubleY();
        setChanged();
        notifyObservers("Y");
    }

    @Override
    public String toString() {
        return point.toString();
    }
}
