package observer;

import java.util.Observable;
import java.util.Observer;

public class PointObserver implements Observer {


    public void update(Observable observable, Object object) {
        System.out.println(object + " changed, new values: " + observable);
    }
}