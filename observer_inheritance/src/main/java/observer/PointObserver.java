package observer;

import java.util.*;

public class PointObserver implements Observer{

  public void update(Observable observable, Object object) {
    Map.Entry entry = (Map.Entry ) object;
    System.out.println(entry.getKey() + " changed, new values: " + entry.getValue());
  }
}