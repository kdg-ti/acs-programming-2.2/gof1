package observer;

import java.util.Observer;

public class ObservablePoint extends Point {
    private MyObservable notifier = new MyObservable();

    public ObservablePoint(int x, int y) {
        super(x, y);
    }

    public void doubleX() {
        super.doubleX();
        notifier.notifyObservers("X",this);
    }

    public void doubleY() {
        super.doubleY();
        notifier.notifyObservers("Y",this);
    }

    public void addObserver(Observer observer) {
        notifier.addObserver(observer);
    }
}
