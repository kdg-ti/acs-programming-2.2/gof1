package adapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter pattern (object adapter)
 * This class implements the HelpDeskQueue interface
 * and translates every action to the methods of ArrayList (= the adaptee)
 */
public class Queue2ListAdapter implements HelpDeskQueue {
	private List<HelpDeskItem> adaptee = new ArrayList<>();

	@Override
	public void enqueue(HelpDeskItem helpDeskItem) {
		adaptee.add(helpDeskItem); //add at the end ( FIFO )
	}

	@Override
	public HelpDeskItem dequeue() {
		if (!adaptee.isEmpty()) {
			return adaptee.remove(0); //always remove from the beginning (FIFO)
		}
		return null;
	}

	@Override
	public void overviewByPriority() {
		List<HelpDeskItem> myList = new ArrayList<>(adaptee);
		myList.sort((item1, item2) -> {
			int difference = item2.getPriority() - item1.getPriority();
			if (difference == 0) {
				difference = item1.getLocalDateTime().compareTo(item2.getLocalDateTime());
			}
			return difference;
		});
		System.out.println("Queue by priority:");
		for (HelpDeskItem helpDeskItem : myList) {
			System.out.println("\t" + helpDeskItem);
		}
	}

	@Override
	public void overviewNatural() {
		System.out.println("Queue in natural order:");
		for (HelpDeskItem helpDeskItem : adaptee) {
			System.out.println("\t" + helpDeskItem);
		}
	}
}
