package adapter;

/**
 Interface that will be implemented by the adapter class.
 */
public interface HelpDeskQueue {

    void enqueue(HelpDeskItem helpDeskItem);

    HelpDeskItem dequeue();

    void overviewByPriority();

    void overviewNatural();
}
