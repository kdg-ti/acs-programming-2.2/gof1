package demo.pos.persistence;

/**
 * @author Jan de Rijke.
 */
public interface RepositoryFactory {
	SaleRepository getSaleRepository();

	ProductCatalog getProductCatalog();
}
