package demo.pos.common;

import java.time.LocalDate;

/**
 * Author: Jan de Rijke
 */
public class Interval {
	LocalDate from;
	LocalDate to;

	public Interval(LocalDate from, LocalDate to) {
		this.from = from;
		this.to = to;
	}

	public boolean contains(LocalDate date){
		return from.isBefore(date) && to.isAfter(date);
	}
}
