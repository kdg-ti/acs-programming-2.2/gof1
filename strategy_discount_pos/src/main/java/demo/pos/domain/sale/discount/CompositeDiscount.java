package demo.pos.domain.sale.discount;

import java.util.ArrayList;
import java.util.List;

public class CompositeDiscount implements Discount{
	List<Discount> discounts=new ArrayList<>();
	@Override
	public double getDiscount(double amount) {
		return discounts.stream().map(dis -> dis.getDiscount(amount)).reduce(0.0,(sum,discount) -> sum + discount );
	}

	public void addDiscount(Discount discount){
		discounts.add(discount);
	}

	public void removeDiscount(Discount discount){
		discounts.remove(discount);
	}
}
