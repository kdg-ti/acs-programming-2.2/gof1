package demo.pos.business;

import demo.pos.domain.product.ProductDescription;
import demo.pos.persistence.ProductCatalog;
import demo.pos.persistence.Repositories;


/**
 * Created by overvelj on 14/11/2016.
 */
public class ProductService {
    private ProductCatalog products = Repositories.getRepositories().getProductCatalog();


    public ProductDescription addProduct(ProductDescription pd) {
        return products.insert( pd);
    }

    public ProductDescription getProductDesc(long id) {
        return products.getById(id);
    }


}
