package demo.pos;

import demo.pos.application.PosController;
import demo.pos.business.ProductService;
import demo.pos.common.Interval;
import demo.pos.domain.product.ProductDescription;
import demo.pos.domain.sale.Sale;
import demo.pos.domain.sale.discount.*;
import demo.pos.persistence.PersistenceType;
import demo.pos.persistence.Repositories;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by overvelj on 14/11/2016.
 */
 class POSTest {
   private PosController r = new PosController();
   private ProductService catalog = new ProductService();

	//   De @BeforeEach methode dient om het systeem te initialiseren voor tests.
    @BeforeEach
     void setUp() {
       // clears the datastore
        Repositories.init(PersistenceType.MEMORY);
	    // Testdata aanmaken. Doe dit nooit in de domeinklassen zelf!
        ProductDescription pd1 = new ProductDescription( 1,"Een", 1.2);
        ProductDescription pd2 = new ProductDescription(2,"Twee", 3.54);
        catalog.addProduct(pd1);
        catalog.addProduct(pd2);

    }

    // Alle testmethoden krijgen een annotation @Test
    @Test
     void createNewSaleTest() {
        Sale s = r.makeNewSale();
        // In testmethoden ga je Asserts uitvoeren. Hierin ga je testen of je systeem zijn verantwoordelijkheden juist uitvoert.
        // Wat test je zoal:
        //   - De postcondities van het operation contract
        //   - De return value en de correcte inhoud ervan.
        //   - Juiste reactie op verificatie stappen die het systeem uitvoert.
        assertNotNull(s);
        assertNotNull(s.getSalesLineItems());
        assertFalse(s.isComplete());
    }

    @Test
     void addItemTest() {
       long saleId =  r.makeNewSale().getId(); // stap 1
        r.enterItem(saleId,1,10); // stap 2
        assertEquals(1,
                r.getSale(saleId).getSalesLineItems().get(0).getPd().getProductId());
    }

    @Test
     void endSaleTest() {
       long saleId =  r.makeNewSale().getId(); // stap 1
        r.enterItem(saleId,1,10);
        r.endSale(saleId);
	    assertTrue(r.getSale(saleId).isComplete());
    }

    @Test
     void getTotalTest() {
       long saleId =  r.makeNewSale().getId(); // stap 1
        r.enterItem(saleId,1,10);
        r.enterItem(saleId,2,5);
        r.endSale(saleId);
	    assertEquals((10*1.2)+(5*3.54),r.getSale(saleId).getTotal());
    }

    @Test
     void makePaymentTest() {
       long saleId =  r.makeNewSale().getId(); // stap 1
       r.enterItem(saleId,1,10);
       r.enterItem(saleId,2,5);
       r.endSale(saleId);
        r.makePayment(saleId,30);
	    assertEquals(30,r.getSale(saleId).getPayment().getAmount());
	    assertEquals(1,r.getStore().countSales());
    }

  @Test

  void DiscountTest() {
    long saleId =  r.makeNewSale().getId(); // stap 1
    r.enterItem(saleId,1,10);
    r.enterItem(saleId,2,5);
    CompositeDiscount discount = new CompositeDiscount();
    discount.addDiscount(new BigSpenderDiscount(27,-5));
    discount.addDiscount(new SaleDiscount(new Interval(LocalDate.now().minusDays(5),LocalDate.now().plusDays(5)),-0.1));
    r.addDiscount(saleId,discount);
    double expectedTotal=(10*1.2)+(5*3.54);
    assertEquals(expectedTotal- 5 - expectedTotal*0.1,r.getSale(saleId).getTotal(),0.01,"Error calculating discount");
  }
}
