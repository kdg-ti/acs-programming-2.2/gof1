package observer;

import java.util.*;

public class PointObserver implements Observer {

	public void update(Observable observable, Object object) {
		ObservablePoint.MyObservable myObservable = (ObservablePoint.MyObservable) observable;
		System.out.println(object + " changed, new values: " + myObservable.getPoint());
	}
}