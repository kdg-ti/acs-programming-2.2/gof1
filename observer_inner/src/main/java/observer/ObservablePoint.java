package observer;

import java.util.*;

public class ObservablePoint extends Point {
    private MyObservable notifier = new MyObservable();

    class MyObservable extends Observable {
        public void notifyObservers(Object object) {
            super.setChanged();
            super.notifyObservers( object );
        }

        public Point getPoint(){
            return ObservablePoint.this;
        }
    }

    public ObservablePoint(int x, int y) {
        super(x, y);
    }

    public void doubleX() {
        super.doubleX();
        notifier.notifyObservers("X");
    }

    public void doubleY() {
        super.doubleY();
        notifier.notifyObservers("Y");
    }

    public void addObserver(Observer observer) {
        notifier.addObserver(observer);
    }
}
