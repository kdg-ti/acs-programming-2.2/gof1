package observer;

public class Point {
    private int x;
    private int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void doubleX() {
        x *= 2;
    }

    public void doubleY() {
        y *= 2;
    }

    @Override
    public String toString() {
        return "(x,y) = (" + x + "," + y + ")";
    }
}
