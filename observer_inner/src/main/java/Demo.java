import observer.ObservablePoint;
import observer.PointObserver;

public class Demo {
    public static void main(String[] args) {
        ObservablePoint point = new ObservablePoint(1, 2);
        PointObserver observer = new PointObserver();
        point.addObserver(observer);

        point.doubleX();
        point.doubleY();
    }
}
